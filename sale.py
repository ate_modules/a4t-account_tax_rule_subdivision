# -*- coding: utf-8 -*-
# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
import logging
from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.pyson import Eval

_logger = logging.getLogger(__name__)

__all__ = ['SaleLine']


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    @fields.depends('sale', 'product', '_parent_sale.warehouse',
        '_parent_sale.shipment_address')
    def _get_tax_rule_pattern(self):
        pool = Pool()
        Location = pool.get('stock.location')

        pattern = super(SaleLine, self)._get_tax_rule_pattern()

        from_subdivision, to_subdivision, account_category = None, None, None
        if self.id is None or self.id < 0:
            warehouse = self.get_warehouse('warehouse')
            if warehouse:
                warehouse = Location(warehouse)
        else:
            warehouse = self.warehouse
        if warehouse and warehouse.address:
            from_subdivision = warehouse.address.subdivision
        if self.sale and self.sale.shipment_address:
            to_subdivision = self.sale.shipment_address.subdivision
        if self.product and self.product.account_category:
            account_category = self.product.account_category

        pattern['from_subdivision'] = (
            from_subdivision.id if from_subdivision else None)
        pattern['to_subdivision'] = (
            to_subdivision.id if to_subdivision else None)
        pattern['account_category'] = (
            account_category.id if account_category else None)
        return pattern
