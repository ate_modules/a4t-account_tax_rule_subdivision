Account Tax Rule Subdivision
############################

The account_taxe_rule_subdivision module extends the tax rule to add origin
and destination subdivision and the acconting category of product as criteria.

Tax Rule Line
*************

Three criteria fields are added:

- From Subdivision: The subdivision of origin
- To Subdivision: The subdivision of destination
- Account Category: The account category of product

The countries are picked from the origin document:

- Sale:

  - The origin subdivision comes from the address of the warehouse.
  - The destination subdivision comes from the shipping address.
  - The account category comes from the saling product.

- Purchase:

  - The origin country comes from the invoice address.
  - The destination country comes from the address of the warehouse.
  - The account category comes from the purchasing product

- Stock Consignment:

  - The origin country comes from the warehouse's address of the location or
    the delivery address for returned customer shipment.
  - The destination country comes from the warehouse's address of the location
    or the delivery address for customer shipment.
  - The account category comes from the delivery or returning product.
