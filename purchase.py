# -*- coding: utf-8 -*-
# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
import logging
from trytond.pool import PoolMeta
from trytond.model import fields

__all__ = ['PurchaseLine']


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @fields.depends('purchase', 'product', '_parent_purchase.warehouse',
        '_parent_purchase.invoice_address')
    def _get_tax_rule_pattern(self):
        pattern = super(PurchaseLine, self)._get_tax_rule_pattern()

        from_subdivision, to_subdivision, account_category = None, None, None
        if self.purchase:
            if self.purchase.invoice_address:
                from_subdivision = self.purchase.invoice_address.subdivision
            warehouse = self.purchase.warehouse
            if warehouse and warehouse.address:
                to_subdivision = warehouse.address.subdivision
            if self.product and self.product.account_category:
                account_category = self.product.account_category

        pattern['from_subdivision'] = (
            from_subdivision.id if from_subdivision else None)
        pattern['to_subdivision'] = (
            to_subdivision.id if to_subdivision else None)
        pattern['account_category'] = (
            account_category.id if account_category else None)
        return pattern
