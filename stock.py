# -*- coding: utf-8 -*-
# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
import logging
from trytond.pool import PoolMeta, Pool
from trytond.model import fields


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @fields.depends('from_location', 'to_location', 'product', 'origin')
    def _get_tax_rule_pattern(self):
        pool = Pool()
        ShipmentOut = pool.get('stock.shipment.out')
        ShipmentOutReturn = pool.get('stock.shipment.out.return')

        pattern = super(Move, self)._get_tax_rule_pattern()

        from_subdivision, to_subdivision, account_category = None, None, None
        if self.from_location.warehouse:
            if self.from_location.warehouse.address:
                from_subdivision = (
                    self.from_location.warehouse.address.subdivision)
        elif isinstance(self.origin, ShipmentOutReturn):
            from_subdivision = self.origin.delivery_address.subdivision
        if self.to_location.warehouse:
            if self.to_location.warehouse.address:
                to_subdivision = self.to_location.warehouse.address.subdivision
        elif isinstance(self.origin, ShipmentOut):
            to_subdivision = self.origin.delivery_address.subdivision
        if self.product and self.product.account_category:
                account_category = self.product.account_category

        pattern['from_subdivision'] = (
            from_subdivision.id if from_subdivision else None)
        pattern['to_subdivision'] = (
            to_subdivision.id if to_subdivision else None)
        pattern['account_category'] = (
            account_category.id if account_category else None)
        return pattern
