# -*- coding: utf-8 -*-
# This file is part of Sale Tax Included Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import logging
from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.pyson import Eval

_logger = logging.getLogger(__name__)

__all__ = ['TaxRuleLineTemplate', 'TaxRuleLine', 'InvoiceLine']


class TaxRuleLineTemplate(metaclass=PoolMeta):
    __name__ = 'account.tax.rule.line.template'
    from_subdivision = fields.Many2One('country.subdivision',
        'From Subdivision', ondelete='RESTRICT', domain=[
            ('from_country', '=', Eval('from_country', -1)),
            ('parent', '=', None),
            ],
        depends=['active', 'from_country'])
    to_subdivision = fields.Many2One('country.subdivision',
        'To Subdivision', ondelete='RESTRICT', domain=[
            ('to_country', '=', Eval('to_country', -1)),
            ('parent', '=', None),
            ],
        depends=['active', 'to_country'])
    account_category = fields.Many2One('product.category', 'Account Category',
        domain=[
            ('accounting', '=', True),
            ])

    def _get_tax_rule_line_value(self, rule_line=None):
        value = super(TaxRuleLineTemplate, self)._get_tax_rule_line_value(
            rule_line=rule_line)
        if not rule_line or rule_line.from_subdivision != self.from_subdivision:
            value['from_subdivision'] = (
                self.from_subdivision.id if self.from_subdivision else None)
        if not rule_line or rule_line.to_subdivision != self.to_subdivision:
            value['to_subdivision'] = (
                self.to_subdivision.id if self.to_subdivision else None)
        if not rule_line or rule_line.account_category != self.account_category:
            value['account_category'] = (
                self.account_category.id if self.account_category else None)
        return value


class TaxRuleLine(metaclass=PoolMeta):
    __name__ = 'account.tax.rule.line'
    from_subdivision = fields.Many2One('country.subdivision',
        'From Subdivision', ondelete='RESTRICT')
    to_subdivision = fields.Many2One('country.subdivision', 'To Subdivision',
        ondelete='RESTRICT')
    account_category = fields.Many2One('product.category', 'Account Category',
        domain=[
            ('accounting', '=', True),
            ])


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @fields.depends('origin')
    def _get_tax_rule_pattern(self):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        PurchaseLine = pool.get('purchase.line')

        pattern = super(InvoiceLine, self)._get_tax_rule_pattern()

        from_subdivision, to_subdivision, account_category = None, None, None
        if isinstance(self.origin, SaleLine):
            if self.origin.warehouse.address:
                from_subdivision = self.origin.warehouse.address.subdivision
            to_subdivision = self.origin.sale.shipment_address.subdivision
        elif isinstance(self.origin, PurchaseLine):
            from_subdivision = self.origin.purchase.invoice_address.subdivision
            if self.origin.purchase.warehouse.address:
                to_subdivision = (
                    self.origin.purchase.warehouse.address.subdivision)
        if (self.origin and self.origin.product
                and self.origin.product.account_category):
            account_category = self.product.account_category

        pattern['from_subdivision'] = (
            from_subdivision.id if from_subdivision else None)
        pattern['to_subdivision'] = (
            to_subdivision.id if to_subdivision else None)
        pattern['account_category'] = (
            account_category.id if account_category else None)
        return pattern
