# -*- coding: utf-8 -*-
# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import account
from . import sale
from . import purchase
from . import stock


def register():
    Pool.register(
        account.TaxRuleLineTemplate,
        account.TaxRuleLine,
        module='account_tax_rule_subdivision', type_='model')
    Pool.register(
        account.InvoiceLine,
        module='account_tax_rule_subdivision', type_='model',
        depends=['account_invoice'])
    Pool.register(
        sale.SaleLine,
        module='account_tax_rule_subdivision', type_='model',
        depends=['sale'])
    Pool.register(
        purchase.PurchaseLine,
        module='account_tax_rule_subdivision', type_='model',
        depends=['purchase'])
    Pool.register(
        stock.Move,
        module='account_tax_rule_subdivision', type_='model',
        depends=['stock'])
